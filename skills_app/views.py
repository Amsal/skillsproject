import requests
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from gsearch.googlesearch import search
from scrape_linkedin import ProfileScraper

from skills_app.models import Student, Skill
from skills_app.serializer import StudentSerializer, SkillSerializer, StudentSkillSerializer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class SkillViewSet(viewsets.ModelViewSet):
    queryset = Skill.objects.all()
    serializer_class = SkillSerializer


class StudentSkillViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = StudentSkillSerializer

    def get_queryset(self):
        return Student.objects.filter(user__username=self.kwargs['username'])


class StudentDirectoryLookup(APIView):
    def get(self, request, format=None):

        username = request.query_params['username']
        cookie = request.query_params['cookie']

        std = None
        response = requests.get('{}/{}/'.format('https://apis.scottylabs.org/directory/v1/andrewID', username))
        if response.status_code == 200:
            jsn = response.json()

            try:
                std = Student.objects.get(user__username=username)
            except Student.DoesNotExist:
                std = None

            if not std:
                try:
                    user = User.objects.get(username=username)
                except User.DoesNotExist:
                    user = User.objects.create(username=username)

                std = Student.objects.create(user=user)

            std.user.last_name = jsn['last_name']
            std.user.first_name = jsn['first_name']
            std.user.email = jsn['preferred_email']
            std.user.save()

        else:
            return Response({'message': 'Unable to fetch details for {}'.format(username)})

        if 'middle_name' in jsn:
            full_name = '{} {} {}'.format(jsn['first_name'], jsn['middle_name'], jsn['last_name'])
        else:
            full_name = '{} {}'.format(jsn['first_name'], jsn['last_name'])

        results = search('{} "Carnegie Mellon University" site:linkedin.com'.format(full_name))

        if not results:
            return Response({'message': 'Something went wrong. Please try again.'})

        if results[0][1].find('linkedin') > 0:
            linkedin_username = results[0][1].split('/')[-1]
        else:
            return Response({'message': 'Unable to LinkedIn Profile for {}'.format(username)})

        with ProfileScraper(cookie=cookie, timeout=30) as scraper:
            profile = scraper.scrape(user=linkedin_username)

        saved_skills = Skill.objects.values_list('name', flat=True)

        for skill in profile.skills:
            sk = None
            if skill['name'] not in saved_skills:
                sk = Skill.objects.create(name=skill['name'])
            else:
                sk = Skill.objects.get(name=skill['name'])

            std.skills.add(sk)

        std.save()

        return Response({'username': linkedin_username, 'search_results': results, 'linkedin_data': profile.to_dict()})
