from django.contrib.auth.models import User
from rest_framework import serializers

from skills_app.models import Student, Skill


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('last_name', 'first_name', 'username', 'email', 'is_staff')


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ('pk', 'name', 'description')


class StudentSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    skills = serializers.PrimaryKeyRelatedField(many=True, queryset=Skill.objects.all())

    class Meta:
        model = Student
        fields = ('pk', 'user', 'skills')

    def create(self, validated_data):
        skills = validated_data.pop('skills')
        user_data = validated_data.pop('user')
        user = User.objects.create(**user_data)
        student = Student.objects.create(user=user)
        student.skills.add(*skills)
        return student


class StudentSkillSerializer(serializers.ModelSerializer):
    skills = serializers.StringRelatedField(many=True)

    class Meta:
        model = Student
        fields = ('skills',)
