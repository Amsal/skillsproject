from django.contrib.auth.models import User
from django.db import models

from django.utils.translation import ugettext as _


class Student(models.Model):
    user = models.OneToOneField(User, verbose_name=_('User'), on_delete=models.CASCADE)
    skills = models.ManyToManyField('Skill', verbose_name=_('Skills'), blank=True)


class Skill(models.Model):
    name = models.CharField(_('Name'), max_length=200, unique=True)
    description = models.TextField(_('Description'), null=True, blank=True)

    def __str__(self):
        return self.name
