# Student Skills App

The project is designed to fetch, store and output skills of a student at CMU.

### Prerequisites

- The project requires python 3.4+ installed on your machine which can be downloaded from (https://www.python.org/downloads)
- Parsing requires ChromeDriver to be installed and added to environment variables.
(http://chromedriver.chromium.org/downloads)
- Root certificates for ssl module are installed on python.
- The project currently supports SQLite and MySQL.

Download links are:
- https://www.sqlite.org/download.html 
- https://dev.mysql.com/downloads/mysql/

The default DB engine is SQLite. You can also connect it with MySQL DB.

After filling the required parameters, overwrite the following connection string in your skillsproject/settings.py file.

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '{your_db_name}',
        'USER': '{your_db_username}',
        'PASSWORD': '{your_db_passowrd}',
        'HOST': '{your_db_url}',
        'PORT': '3306',
    }
}
```

## Getting Started

- Create a virtual environment. Learn more (https://docs.python.org/3/tutorial/venv.html)
- Activate the virtual environment.
- Install requirement by executing the following command.

```
pip install -r requirements.txt
```

- Run migrations by executing the following command.
```
python manage.py migrate
```


### Running

- Once the migration is complete. Run the project by running the following command. 

```
python manage.py runserver
```

You can access the site from your browser by visiting the following URL.

```
http://localhost:8000/
```

- Students and Skills can be added by accessing the /student/ and /skill/ endpoints respectively.

- The endpoint
```
/get_skills/{student_username}/
```

will return a list of skills the given student have.

- The endpoint
```
/student_lookup/?username={student_username}&cookie={cookie='your_li_at_value'}/
```

will connect to ScottyLabs Directory API to fetch the name and basic details of a student. The name is then sent to google to find their LinkedIn profile. Then the parser hits the LinkedIn to fetch the skills of the student and store it in the database.

Because of Linkedin's anti-scraping measures, we have to show LinkedIn that the code is a real user. To do this, you need to add the li_at cookie as a parameter to the endpoint.

Make sure that ChromeDriver is added to environment variables before calling this endpoint.

## Getting LI_AT Cookie
- Navigate to www.linkedin.com and log in.
- Open browser developer tools (Ctrl-Shift-I or right click -> inspect element).
- Select the appropriate tab for your browser (Application on Chrome, Storage on Firefox).
- Click the Cookies dropdown on the left-hand menu, and select the www.linkedin.com option.
- Find and copy the li_at value and pass it to the endpoint.

## Built With

* [Python 3](https://www.python.org/) - The programming language
* [Django 1.11.6](https://www.djangoproject.com/) - The web framework used
* [Django Rest Framework 3.9.0](https://www.django-rest-framework.org/) - Toolkit for building Web APIs in Django
* [Python Google Search API 1.6.0](https://github.com/aviaryan/python-gsearch/) - Unofficial Google Search API for Python
* [Scrape LinkedIn 0.3.3](https://github.com/austinoboyle/scrape-linkedin-selenium/) - Python package to scrape details from public LinkedIn profiles

## Authors

* **Muhammad Amsal Naseem** - [Amsal](https://bitbucket.org/Amsal/)
