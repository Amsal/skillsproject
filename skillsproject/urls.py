"""skillsproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from skills_app.views import StudentViewSet, SkillViewSet, StudentSkillViewSet, StudentDirectoryLookup

schema_view = get_swagger_view(title='Student Skill API')

router = routers.DefaultRouter()
router.register(r'student', StudentViewSet, base_name='student_viewset')
router.register(r'skill', SkillViewSet, base_name='skill_viewset')

urlpatterns = [

    # url(r'^$', schema_view),
    url(r'^', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^get_skills/(?P<username>\w+)/$', StudentSkillViewSet.as_view({'get': 'list'}), name='student_skill_view'),
    url(r'^student_lookup/$', StudentDirectoryLookup.as_view(), name='student_directory_lookup'),


]
